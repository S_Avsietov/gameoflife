# GameOfLife
## How to make jar file from command line
1. Go to the project folder

   \>cd d:\java\projects\GameOfLife  

  2. Create folder bin
...\>md bin
  3. Copy manifest.mf to folder bin
...\>copy manifest.mf bin\manifest.mf
  4. Go to folder bin
...\>cd bin
  5. Compile text files of classes **.java into binary **.class:
...\>javac -d bin -encoding UTF-8 src/gameoflife/*.java
  6. Add binary class files and manifest file to the jar archive  
...\>jar -cfm GameOfLife.jar manifest.mf gameoflife/*.class
