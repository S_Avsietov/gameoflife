package gameoflife;

import javafx.animation.AnimationTimer;

/**
 * Класс, организующий игровые циклы
 *
 * @author Сергей
 */
public class GameLoop extends AnimationTimer {

    private final GameController controller;
    private int frame;

    public GameLoop(GameController controller) {
        this.controller = controller;
        frame = 1;
    }

    public void initialize() {
        controller.clearAndRedraw();
    }

    @Override
    public void handle(long now) {
        frame++;
        //Рисовать каждый 10 кадр (всего 60 кадров в секунду)
        if (frame == 10) {
            controller.moveAndRedraw();
            frame = 1;
        }
    }

    @Override
    public void start() {
        super.start();
    }

    @Override
    public void stop() {
        super.stop();
    }

}
