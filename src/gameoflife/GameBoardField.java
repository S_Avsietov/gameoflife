package gameoflife;

import java.util.Arrays;
import java.util.List;

/**
 * Класс, описывающий поле на игровой доске
 *
 * @author Сергей
 */
public class GameBoardField {

    private final int column;//Номер поля по горизонтали на игровой доске
    private final int row;//Номер поля по вертикали на игровой доске

    public GameBoardField(int column, int row) {
        this.column = column;
        this.row = row;
    }

    /**
     * Получить номер поля по горизонтали
     *
     * @return - номер поля по горизонтали
     */
    public int getColumn() {
        return column;
    }

    /**
     * Получить номер поля по вертикали
     *
     * @return - номер поля по вертикали
     */
    public int getRow() {
        return row;
    }

    /**
     * Переопределение метода equals для корректной работы с HashMap
     *
     * @param obj - сравниваемый объект
     * @return - TRUE если равны, FALSE если не равны
     */
    @Override
    public boolean equals(Object obj) {
        //Проверить, что ссылочный параметр не ссылается на тот же объект
        if (this == obj) {
            return true;
        }

        //Проверить, что ссылочный параметр содержит ссылку
        if (obj == null) {
            return false;
        }

        //Проверить что ссылочный параметр указывает на объекто того же класса
        if (getClass() != obj.getClass()) {
            return false;
        }

        //Если объекты одного класса,То явно приводим к классу
        //GameBoardPoint и сравниваем значения атрибутов
        GameBoardField other = (GameBoardField) obj;

        return this.column == other.column && this.row == other.row;
    }

    /**
     * Переопределение метода hashCode для корректной работы с HashMap
     *
     * @return - хеш объекта
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + this.column;
        hash = 41 * hash + this.row;
        return hash;
    }

    /**
     * Сгенерить список соседних клеток
     *
     * @return - список соседний клеток
     */
    public List<GameBoardField> generateAdjacentFields() {
        return Arrays.asList(
                new GameBoardField(column - 1, row + 1),
                new GameBoardField(column, row + 1),
                new GameBoardField(column + 1, row + 1),
                new GameBoardField(column - 1, row),
                new GameBoardField(column + 1, row),
                new GameBoardField(column - 1, row - 1),
                new GameBoardField(column, row - 1),
                new GameBoardField(column + 1, row - 1)
        );
    }
}
