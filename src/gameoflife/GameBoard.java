package gameoflife;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author Сергей
 */
public class GameBoard extends Canvas {

    private final static int LINE_WIDTH = 1;
    private GraphicsContext graphicsContext;
    private final int cellWidth;
    private final int cellHeight;

    /**
     * Конструктор
     *
     * @param cellWidth - ширина ячейки
     * @param cellHeight -высота ячейки
     * @param strokeColor -цвет линии
     * @param fillColor - цвет заливки
     */
    private GameBoard(int cellWidth,
            int cellHeight,
            Color strokeColor,
            Color fillColor
    ) {
        this(0, 0, cellWidth, cellHeight, strokeColor, fillColor);
    }

    /**
     * Конструктор
     *
     * @param boardWidth - ширина поля
     * @param boardHeight - высота поля
     * @param cellWidth - ширина ячейки
     * @param cellHeight -высота ячейки
     * @param strokeColor -цвет линии
     * @param fillColor - цвет заливки
     */
    private GameBoard(int boardWidth,
            int boardHeight,
            int cellWidth,
            int cellHeight,
            Color strokeColor,
            Color fillColor) {
        super(boardWidth, boardHeight);
        this.cellWidth = cellWidth;
        this.cellHeight = cellHeight;
    }

    /**
     * Фабричный метод создания экземпляра доски
     *
     * @param cellWidth - ширина ячейки
     * @param cellHeight -высота ячейки
     * @param strokeColor -цвет линии
     * @param fillColor - цвет заливки
     * @return - экземпляр доски
     */
    public static GameBoard createGameBoard(int cellWidth,
            int cellHeight,
            Color strokeColor,
            Color fillColor) {
        GameBoard gameBoard = new GameBoard(
                cellWidth,
                cellHeight,
                strokeColor,
                fillColor);

        gameBoard.graphicsContext = gameBoard.getGraphicsContext2D();
        gameBoard.graphicsContext.setStroke(strokeColor);
        gameBoard.graphicsContext.setFill(fillColor);
        gameBoard.graphicsContext.setLineWidth(GameBoard.LINE_WIDTH);
        return gameBoard;
    }

    /**
     * Метод возвращающий максимальный номер ячейки по горизонтали
     *
     * @return максимальный номер ячейки по горизонтали
     */
    public int getMaxColumn() {
        return (int) getWidth() / cellWidth - 1;
    }

    /**
     * Метод возвращающий максимальный номер ячейки по вертикали
     *
     * @return максимальный номер ячейки по вертикали
     */
    public int getMaxRow() {
        return (int) getHeight() / cellHeight - 1;
    }

    /**
     * Перевести координату Х в номер ячейки по горизонтали
     *
     * @param x - координата х
     * @return - номер ячейки по горизонтали
     */
    public int convertX2column(double x) {
        return (int) x / cellWidth;
    }

    /**
     * Перевести координату Y в номер ячейки по вертикали
     *
     * @param y - координата y
     * @return - номер ячейки по вертикали
     */
    public int convertY2row(double y) {
        return (int) y / cellHeight;
    }

    /**
     * Метод для рисования координатной сетки на холсте
     */
    public void redrawGrid() {
        //Очистить холст
        graphicsContext.clearRect(0, 0, getWidth(), getHeight());

        //Нарисовать вертикальные линии
        for (int x = 0; x <= getWidth(); x += cellWidth) {
            graphicsContext.strokeLine(x, 0, x, getHeight());
        }

        //Нарисовать горизонтальные линии
        for (int y = 0; y <= getHeight(); y += cellHeight) {
            graphicsContext.strokeLine(0, y, getWidth(), y);
        }
    }

    /**
     * Метод рисования клетки
     *
     * @param column - строка
     * @param row - столбец
     */
    public void fillCell(int column, int row) {
        graphicsContext.fillRect(column * cellWidth + LINE_WIDTH,
                row * cellHeight + LINE_WIDTH,
                cellWidth - 2 * LINE_WIDTH,
                cellHeight - 2 * LINE_WIDTH);
    }

    /**
     * Метод стирания клетки
     *
     * @param column - строка
     * @param row - столбец
     */
    public void clearCell(int column, int row) {
        graphicsContext.clearRect(column * cellWidth + LINE_WIDTH,
                row * cellHeight + LINE_WIDTH,
                cellWidth - 2 * LINE_WIDTH,
                cellHeight - 2 * LINE_WIDTH);
    }

}
