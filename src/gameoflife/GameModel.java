package gameoflife;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * Класс, содержащий игровые данные и методы по работе с ними
 *
 * @author Сергей
 */
public class GameModel {

    //Список отслеживаемых полей с живыми клетками
    private final Set<GameBoardField> fields;
    //Счетчик окружающих живых клеток для отслеживаемых полей или кандидатов
    private final Map<GameBoardField, Integer> adjacentAliveFieldsCount;
    //Счетчик поколения
    private final IntegerProperty generationNum;
    //Счетчик клеток
    private final IntegerProperty fieldsCount;

    public GameModel() {
        fields = new HashSet<>();
        adjacentAliveFieldsCount = new HashMap<>();
        generationNum = new SimpleIntegerProperty(1);
        fieldsCount = new SimpleIntegerProperty(0);
    }

    /**
     * Метод для очистки модели
     */
    public void clearModel() {
        fields.clear();
        adjacentAliveFieldsCount.clear();
        generationNum.setValue(1);
        fieldsCount.setValue(0);
    }

    /**
     * Добавить поле в список отслеживаемых
     *
     * @param gameBoardField - добавляемое поле
     */
    public void addField(GameBoardField gameBoardField) {
        gameBoardField.generateAdjacentFields().forEach((field) -> {
            increaseCount(field);
        });
        fields.add(gameBoardField);
        fieldsCount.setValue(fields.size());
    }

    /**
     * Удалить поле из списка отслеживаемых
     *
     * @param gameBoardField - удаляемое поле
     */
    public void removeField(GameBoardField gameBoardField) {
        gameBoardField.generateAdjacentFields().forEach((field) -> {
            decreaseCount(field);
        });
        fields.remove(gameBoardField);
        fieldsCount.setValue(fields.size());
    }

    /**
     * Получить список отслежваемых полей
     *
     * @return - множество отслеживаемых полей
     */
    public Set<GameBoardField> getFields() {
        Set<GameBoardField> fieldsCopy = new HashSet<>();
        fields.stream().forEach((field) -> {
            fieldsCopy.add(new GameBoardField(field.getColumn(), field.getRow()));
        });
        return fields;
    }

    /**
     * Получить номер поколения
     *
     * @return int - номер поколения
     */
    public IntegerProperty getGenerationNum() {
        return generationNum;
    }

    public IntegerProperty getFiledsCount() {
        return fieldsCount;
    }

    /**
     * Метод делающий ход в игре
     */
    public void move() {
        ArrayList<GameBoardField> dyingFields = new ArrayList<>();
        ArrayList<GameBoardField> bornFields = new ArrayList<>();
        generationNum.setValue(generationNum.getValue() + 1);

        //Определим, какие клетки к концу хода умрут
        //(без соседей(0) или соседей мало(1) или соседей слишком много(4 и больше))
        fields.forEach((field) -> {
            Integer neighborsCount
                    = adjacentAliveFieldsCount.getOrDefault(field, 0);
            if (neighborsCount < 2 || neighborsCount > 3) {
                dyingFields.add(field);
            }
        });

        //Определяем клетки, которые родятся к концу хода
        //(клетки, у которых число соседей 3 и она не занята)
        adjacentAliveFieldsCount.forEach((field, neighborsCount) -> {
            if (neighborsCount == 3 && !fields.contains(field)) {
                bornFields.add(field);
            }
        });

        //Завершаем ход. Записываем умерших и рожденых
        dyingFields.forEach((field) -> {
            removeField(field);
        });
        bornFields.forEach((field) -> {
            addField(field);
        });
    }

    /**
     * Метод уменьшающий счетчик живых клеток для отслеживаемых полей и
     * кандидатов
     *
     * @param gameBoardField - поле, счечик которого меняем
     */
    private void decreaseCount(GameBoardField gameBoardField) {
        int newNumber = adjacentAliveFieldsCount.getOrDefault(gameBoardField, 0) - 1;
        if (newNumber > 0) {
            adjacentAliveFieldsCount.put(gameBoardField, newNumber);
        } else {
            adjacentAliveFieldsCount.remove(gameBoardField);
        }
    }

    /**
     * Метод увеличивающий счетчик живых клеток для отслеживаемых полей и
     * кандидатов
     *
     * @param gameBoardField - поле, счечик которого меняем
     */
    private void increaseCount(GameBoardField gameBoardField) {
        int newNumber = adjacentAliveFieldsCount.getOrDefault(gameBoardField, 0) + 1;
        adjacentAliveFieldsCount.put(gameBoardField, newNumber);
    }
}
