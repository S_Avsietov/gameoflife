package gameoflife;

import static javafx.scene.input.MouseEvent.MOUSE_CLICKED;

/**
 * Класс, который предназначен для координации работы класса представляющего
 * модель данных и класса отвечающего за визуальное отображение
 *
 * @author Сергей
 */
public class GameController {

    private final GameModel model;
    private final GameBoard board;

    private GameController(GameModel model, GameBoard bord) {
        this.model = model;
        this.board = bord;
    }

    /**
     * Фабричный метод для создания экземпляра GameController
     *
     * @param model - ссылка на игровую модель
     * @param board - ссылка на игровую доску
     * @return - экземпляр GameController
     */
    public static GameController createGameController(
            GameModel model,
            GameBoard board) {
        GameController controller = new GameController(model, board);
        controller.addBoardHandler();
        controller.addBoardListener();
        return controller;
    }

    /**
     * Метод делающий ход и перерисовывающий состояние на доске
     */
    public void moveAndRedraw() {
        model.move();
        board.redrawGrid();
        drawFileds();
    }

    /**
     * Метод возвращающий игру в начальное состоние
     */
    public void clearAndRedraw() {
        model.clearModel();
        board.redrawGrid();
    }

    /**
     * Метод, который инвертирует состояние клетки под точкой (x,y) на игровой
     * доске
     *
     * @param x - координата x точки на игровой доске
     * @param y - координата y точки на игровой доске
     */
    private void reverseFiled(double x, double y) {
        //По координатам XY создадим поле на игровой доске
        GameBoardField field = new GameBoardField(
                board.convertX2column(x),
                board.convertY2row(y)
        );

        //Если поле уже существовало в списке включенных на доске
        //то удаляем его из этого списка и отключаем на доске
        //Иначе добавляем в список и включаем на поле
        if (model.getFields().contains(field)) {
            model.removeField(field);
            board.clearCell(field.getColumn(), field.getRow());
        } else {
            model.addField(field);
            board.fillCell(field.getColumn(), field.getRow());
        }
    }

    /**
     * Отобразить живые(равно отслеживаемы) поля на доске
     */
    private void drawFileds() {
        model.getFields().forEach((field) -> {
            board.fillCell(field.getColumn(), field.getRow());
        });
    }

    /**
     * Метод добавляющий обрабочик событий для игровой доски
     */
    private void addBoardHandler() {
        board.addEventHandler(MOUSE_CLICKED, (e) -> {
            reverseFiled(e.getX(), e.getY());
        });
    }

    /**
     * Метод добавляющий слушатель для игровой доски
     */
    private void addBoardListener() {
        board.widthProperty().addListener((value, oldValue, newValue) -> {
            board.redrawGrid();
            drawFileds();
        });

        board.heightProperty().addListener((value, oldValue, newValue) -> {
            board.redrawGrid();
            drawFileds();
        });
    }
}
