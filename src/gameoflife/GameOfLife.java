package gameoflife;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import static javafx.scene.paint.Color.GREEN;
import static javafx.scene.paint.Color.BLACK;

/**
 *
 * @author Сергей
 */
public class GameOfLife extends Application {

    private GameBoard board;
    private GameModel model;
    private GameLoop gameLoop;
    //TODO-переделать на файл стилей
    private final static String LABEL_STYLE = "-fx-text-fill: green; -fx-font-size: 24px;";
    private final static String BUTTON_STYLE = "-fx-background-color: green";
    private final static String PANE_STYLE = "-fx-background-color: black";

    @Override
    public void init() {
        model = new GameModel();
        board = GameBoard.createGameBoard(15, 15, GREEN, GREEN);
        gameLoop = new GameLoop(
                GameController.createGameController(model, board)
        );

    }

    @Override
    public void start(Stage primaryStage) {

        BorderPane root = new BorderPane();
        root.setStyle(PANE_STYLE);
        root.setCenter(createGameBoard());
        root.setBottom(createBottomBar());

        Scene scene = new Scene(root, 800, 600, BLACK);
        primaryStage.setTitle("Game Of life");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Метод создающий игровое поле
     *
     * @return Node - узел с элементами
     */
    private Node createGameBoard() {
        Pane pane = new Pane();
        board.redrawGrid();
        board.widthProperty().bind(pane.widthProperty());
        board.heightProperty().bind(pane.heightProperty());
        pane.getChildren().add(board);
        return pane;
    }

    /**
     * Метод создающий кнопки
     *
     * @return Node - узел с элементами
     */
    private Node createBottomBar() {
        Insets barInsets = new Insets(5, 10, 5, 10);
        HBox bar = new HBox();
        bar.setAlignment(Pos.CENTER_RIGHT);
        ButtonBar buttonBar = new ButtonBar();
        HBox.setMargin(buttonBar, barInsets);

        Label genNumLabel = new Label();
        HBox.setMargin(genNumLabel, barInsets);
        genNumLabel.setStyle(LABEL_STYLE);
        genNumLabel.textProperty().bind(
                new SimpleStringProperty("Номер поколения: ").
                concat(model.getGenerationNum().asString())
        );

        Label fieldsCountLabel = new Label();
        HBox.setMargin(fieldsCountLabel, barInsets);
        fieldsCountLabel.setStyle(LABEL_STYLE);
        fieldsCountLabel.textProperty().bind(
                new SimpleStringProperty("Число клеток: ").
                concat(model.getFiledsCount().asString())
        );

        Button startButton = new Button("Старт");
        startButton.setStyle(BUTTON_STYLE);

        Button stopButton = new Button("Стоп");
        stopButton.setDisable(true);
        stopButton.setStyle(BUTTON_STYLE);

        Button newButton = new Button("Очистить");
        newButton.setStyle(BUTTON_STYLE);

        buttonBar.getButtons().addAll(
                startButton,
                stopButton,
                newButton);

        bar.getChildren().addAll(fieldsCountLabel, genNumLabel, buttonBar);

        startButton.setOnAction((e) -> {
            gameLoop.start();
            startButton.setDisable(true);
            newButton.setDisable(true);
            stopButton.setDisable(false);
        });

        stopButton.setOnAction((e) -> {
            gameLoop.stop();
            stopButton.setDisable(true);
            startButton.setDisable(false);
            newButton.setDisable(false);
        });

        newButton.setOnAction((e) -> {
            gameLoop.initialize();
        });
        return bar;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
